//
//  Tester.hpp
//  Maximal Splitting Families
//
//  Created by Bryce Frederickson on 11/17/18.
//  Copyright © 2018 Bryce Frederickson. All rights reserved.
//

#ifndef SplitCheck_hpp
#define SplitCheck_hpp

#include <stdio.h>
#include <string>
#include "Iterator.hpp"
#include "Printer.hpp"

class SplitCheck{
    Iterator it;
    Printer p;
public:
    void maximalFamilies(int startingPoint[], int n, int famSize, std::string fileName, int howOftenUpdate, int howOftenCheckForShortcuts);
    int howManySplit(int* F, int n, int famSize);
    int isReducible(int F[], int n, int k);
    std::vector<std::vector<bool>> evenPowerSet(int k);
    bool splits(int* F, int n, int k, const std::vector<std::vector<bool>>& B);
    int canAGreaterPointBeAdded(int F[], int n, int k, int lowerBound, int* FIndices, const std::vector<int>& numbas);
    std::vector<std::vector<bool> > oddPowerSetPlusOne(int k);
};

#endif /* SplitCheck_hpp */
