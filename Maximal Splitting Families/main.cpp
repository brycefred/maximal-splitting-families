//
//  main.cpp
//  Maximal Splitting Families
//
//  Created by Bryce Frederickson on 11/17/18.
//  Copyright © 2018 Bryce Frederickson. All rights reserved.
//

#include <iostream>
#include <time.h>
#include <chrono>
#include "SplitCheck.hpp"

int main(int argc, const char * argv[]) {
    SplitCheck ch;
    /*
     ch.maximalFamilies() takes an array of ints. Each int corresponds to a point, and its binary representation tells which sets contain that point.
     
     For example, the array {0b0, 0b1, 0b10, 0b11} represents the splitting family {'a','b',...} on the set {0,a,b,ab}, where set 'a' contains the first and third points, and set 'b' contains the second and fourth point. Now this splitting family may contain a set 'c', but 'c' is assumed to be empty, so we might say that the family has two 'active' sets. Note that if a point is added to the configuration, our family will no longer be a splitting family, so we say that {0,a,b,ab} is maximal.
     
     The algorithm iterates through maximal families by attempting to add points to existing configurations, then incrementing in a systematic way that allows for shortcuts that take into account the fact that sets can be relabeled and complemented without changing whether or not the set is a splitting family.
     
     To implement ch.maximalFamilies(int startingPoint[], int n, int famSize, std::string fileName, int howOftenCheckForShortcuts), you should know what each of the arguments are.
     
     int startingPoint[] indicates the first family to be checked, then continue from there.
     This allows you to stop the program and pick it up where you left off, if needed. Because arrays are of fixed size, make it large enough that you can be sure that it won't use the whole thing. Make the entries you don't care about -1 so it knows to ignore those. For example, if you want to start with {0,a,b,ab}, set startingPoint[] to {0,b1,b10,b11,-1,-1,-1,...} If you want to check all splitting families, start with {0,-1,-1,-1,...}
     
     int n puts a cap on the number of sets you allow. If you're checking splitting families on <= 5 sets, set n = 5.
     
     int famSize is the size of startingPoint[].
     
     std::string fileName is the name of the output file. You may need to specify the location as well. The program outputs all families of size >= 2n and checks if they are isomorphic to the standard Coppersmith family. Printing to the console is mostly unrelated. The console gives a periodic update and a 'Large Family' alert when something is printed to the file.
     
     int howOftenUpdate controls how often (in terms of number of iterations) you want it to give update to console.
     
     int howOftenCheckForShortcuts controls how often you want it to check for shortcuts, keeping in mind that it takes nearly as much computation to check for a shortcut as it does to do one iteration. Certain stretches of the sequence have more available shortcuts than others. In particular, shortcuts become more common as it goes
     */
    
    // sample1 checks all possible splitting families with 4 sets, starting with {0}
    std::chrono::steady_clock::time_point begin1 = std::chrono::steady_clock::now();
    int sample1[16];
    sample1[0] = 0;
    for(int i = 1; i < 16; i++){
        sample1[i] = -1;
    }
    ch.maximalFamilies(sample1, 4, 16, "sample1.txt",20,5);
    std::chrono::steady_clock::time_point end1= std::chrono::steady_clock::now();
    std::cout << "Time elapsed for sample 1: " << std::chrono::duration_cast<std::chrono::microseconds>(end1 - begin1).count() / 1000000.0 << " seconds." << std::endl << std::endl;
    
    // sample2 checks splitting families that would come after {0,ab,acd}, with 6 sets
    std::chrono::steady_clock::time_point begin2 = std::chrono::steady_clock::now();
    int sample2[16];
    sample2[0] = 0b0;
    sample2[1] = 0b11;
    sample2[2] = 0b1101;
    for (int i = 3; i < 17; i++) {
        sample2[i] = -1;
    }
    ch.maximalFamilies(sample2, 6, 16, "sample2.txt", 500,50);
    std::chrono::steady_clock::time_point end2= std::chrono::steady_clock::now();
    std::cout << "Time elapsed for sample 2: " << std::chrono::duration_cast<std::chrono::microseconds>(end2 - begin2).count() / 1000000.0 << " seconds." << std::endl;
    return 0;
}
