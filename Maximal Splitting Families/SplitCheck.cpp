//
//  Tester.cpp
//  Maximal Splitting Families
//
//  Created by Bryce Frederickson on 11/17/18.
//  Copyright © 2018 Bryce Frederickson. All rights reserved.
//

#include "SplitCheck.hpp"
#include "HyperCubeGraph.hpp"
#include <string>
#include <vector>
#include <iostream>
#include <cmath>

void SplitCheck::maximalFamilies(int startingPoint[], int n, int famSize, std::string fileName, int howOftenUpdate, int howOftenCheckForShortcuts) {
    // Initialize variables
    int* family = startingPoint;
    long long count = 0;
    // Numbas gives optimal incrementation method: 0 a b c d e f g ab ac ad ae af ag bc ...
    std::vector<bool> chooseOrder(n);
    std::vector<int> numbas;
    for (int l = 0; l <= n; l++) {
        for (int i = 0; i < n; i++) {
            chooseOrder[i] = (i < l);
        }
        do {
            int x = 0;
            for (int i = 0; i < n; i++) {
                if (chooseOrder[i]) {
                    x += (1<<i);
                }
            }
            numbas.push_back(x);
        } while(it.nextCombo(chooseOrder));
    }
    // Inverse function of numbas
    std::vector<int> numbasInv;
    for (int i = 0; i < (1<<n); i++) {
        int j = 0;
        while (numbas[j] != i) {
            j++;
        }
        numbasInv.push_back(j);
    }
    bool workingFile = true;
    // Each binary string corresponds to a point, and the 1's tell which sets contain it
    // The last position corresponds to set a, the second to last to set b, etc.
    // Given a number 0b1011, gives index of 0b1011 = abd in numbas
    int familyIndices[famSize];
    for (int i = 0; i < famSize; i++) {
        if (family[i] >= 0) {
            familyIndices[i] = numbasInv[family[i]];
        } else {
            familyIndices[i] = familyIndices[i-1] + 1;
        }
    }
    for (int i = 0; i < famSize; i++) {
        family[i] = numbas[familyIndices[i]];
    }
    std::cout << "Starting family:\n";
    p.printFamily(family,n,famSize);
    std::cout << "\n";
    int maxSize = 0;
    int mod = 10;
    int k = howManySplit(family,n,famSize);
    int min = familyIndices[k];
    int wait = 0;
    while (family[0] == 0) {
        // short cut
        if (count % mod == 0) {
            HyperCubeGraph g;
            int shortCutIndex = isReducible(family,n,k);
            int otherShortCutIndex = g.minimumDistanceLessThan(family, n, k, g.distance(n, family[0], family[1]));
            if (shortCutIndex != -1 && otherShortCutIndex != -1 && otherShortCutIndex < shortCutIndex) {
                shortCutIndex = otherShortCutIndex;
            } else if (shortCutIndex == -1) {
                shortCutIndex = otherShortCutIndex;
            }
            if (shortCutIndex != -1) {
                if (familyIndices[shortCutIndex] != (1<<n)-1) {
                    it.nextCombo(familyIndices,(1<<n),shortCutIndex+1);
                    family[shortCutIndex] = numbas[familyIndices[shortCutIndex]];
                    if (!splits(family,n,shortCutIndex+1,evenPowerSet(shortCutIndex+1))) {
                        k = shortCutIndex;
                        min = familyIndices[k];
                    } else {
                        k = shortCutIndex + 1;
                        min = familyIndices[k-1]+1;
                    }
                } else {
                    it.nextCombo(familyIndices,(1<<n),shortCutIndex+1);
                    for (int i = 0; i < famSize; i++) {
                        family[i] = numbas[familyIndices[i]];
                    }
                    int oldK = k;
                    k = howManySplit(family,n,oldK);
                    if (oldK == k) {
                        min = familyIndices[k-1]+1;
                    } else {
                        min = familyIndices[k];
                    }
                }
                
                mod = howOftenCheckForShortcuts;
                wait = 0;
            } else {
                mod = 1;
                //                if (wait > 200) {
                //                    if (wait > 1000) {
                //                        if (wait > 5000) {
                //                            mod = 2500;
                //                        } else {
                //                            mod = 500;
                //                        }
                //                    } else {
                //                        mod = 100;
                //                    }
                //                } else {
                //                    mod = 30;
                //                }
                //                wait++;
            }
        }
        // Add points until I can't anymore
        int extra;
        while ((extra = canAGreaterPointBeAdded(family,n,k,min,familyIndices,numbas))) {
            familyIndices[k] = extra;
            family[k] = numbas[familyIndices[k]];
            min = extra+1;
            k++;
        }
        family[k] = numbas[familyIndices[k]];
        // Update maxSize
        if (k > maxSize) {
            maxSize = k;
            std::cout << "Max size: " << maxSize << "\n";
        }
        // Make note of large families
        if (k >= 2*n) {
            p.printFamilyToFileCoppersmith(family,n,k,fileName);
            std::cout << "Large family:\n";
            p.printFamily(family,n,k);
            std::cout << "\n\n";
        }
        // Give a periodic update
        count++;
        if (count % howOftenUpdate == 0) {
            std::cout << "Current family:\n";
            p.printFamily(family,n,k);
            std::cout << "Count: " << count << "\n";
            std::cout << "Max size: " << maxSize << "\n\n";
            if (!workingFile) {
                std::cout << "File not working.\n";
            }
        }
        // Move on to next family
        if (familyIndices[k-1] != (1<<n)-1) {
            it.nextCombo(familyIndices,(1<<n),k);
            family[k-1] = numbas[familyIndices[k-1]];
            if (!splits(family,n,k,oddPowerSetPlusOne(k))) {
                k--;
            }
        } else {
            it.nextCombo(familyIndices,(1<<n),k);
            for (int i = 0; i < famSize; i++) {
                family[i] = numbas[familyIndices[i]];
            }
            int oldK = k;
            k = howManySplit(family,n,oldK);
            if (oldK == k) {
                min = familyIndices[k-1]+1;
            } else {
                min = familyIndices[k];
            }
        }
        min = familyIndices[k];
    }
    std::cout << "Count: " << count << "\n";
}

int SplitCheck::howManySplit(int* F, int n, int famSize) {
    int k = 1;
    while (splits(F,n,k,evenPowerSet(k)) && k <= famSize) {
        k++;
    }
    return k-1;
}

int SplitCheck::isReducible(int F[], int n, int k) {
    std::vector<int> symmetries;
    symmetries.push_back((1<<n) - 1);
    for (int j = 0; j < k; j++) {
        for (int l = 0; l < symmetries.size(); l++) {
            bool danger = false;
            for (int i = 0; i < n; i++) {
                if (!danger) {
                    danger = (((symmetries[l] >> i) & 1) && !((F[j] >> i) & 1));
                } else if (((symmetries[l] >> i) & 1) && ((F[j] >> i) & 1)) {
                    return j;
                }
            }
        }
        for (int l = 0; l < symmetries.size(); l++) {
            int copy = symmetries[l];
            for (int i = 0; i < n; i++) {
                if (((symmetries[l] >> i) & 1) && !((F[j] >> i) & 1)) {
                    copy ^= (1<<i);
                }
            }
            if (copy != symmetries[l] && copy != 0) {
                symmetries.push_back(copy ^ symmetries[l]);
                symmetries[l] = copy;
            }
        }
        
    }
    return -1;
}

std::vector<std::vector<bool>> SplitCheck::evenPowerSet(int k) {
    std::vector<std::vector<bool>> ret;
    ret.reserve(pow(2,k));
    for (int l = k - (k % 2); l >= 0; l -= 2) {
        std::vector<bool> currentColumn(k);
        for (int i = 0; i < k; i++) {
            currentColumn[i] = (i < l);
        }
        do {
            ret.push_back(currentColumn);
        } while (it.nextCombo(currentColumn));
    }
    return ret;
}

bool SplitCheck::splits(int* F, int n, int k, const std::vector<std::vector<bool>>& B) {
    bool ret = true;
    if (k != B[0].size()) {
        ret = false;
    } else {
        for (std::vector<bool> row : B) {
            ret = false;
            for (int i = 0; i < n && !ret; i++) {
                int dot = 0;
                for (int j = 0; j < k; j++) {
                    if (row[j]) {
                        if ((((F[j]) >> i) & 1)) {
                            dot++;
                        } else {
                            dot--;
                        }
                    }
                }
                ret = (ret || (dot == 0));
            }
            if (!ret) {
                break;
            }
        }
    }
    return ret;
}

int SplitCheck::canAGreaterPointBeAdded(int F[], int n, int k, int lowerBound, int* FIndices, const std::vector<int>& numbas) {
    int tester = lowerBound;
    while (tester < numbas.size()) {
        F[k] = numbas[tester];
        if (splits(F,n,k+1,oddPowerSetPlusOne(k+1))) {
            return tester;
        }
        tester++;
    }
    return 0;
}

std::vector<std::vector<bool> > SplitCheck::oddPowerSetPlusOne(int k) {
    std::vector<std::vector<bool>> ret;
    ret.reserve((1<<k));
    for (int l = ((k % 2) ? k-2 : k-1); l >= 0; l -= 2) {
        std::vector<bool> currentColumnAlmost(k-1);
        for (int i = 0; i < k-1; i++) {
            currentColumnAlmost[i] = (i < l);
        }
        std::vector<bool> currentColumn(k);
        currentColumn[k-1] = 1;
        do {
            for (int i = 0; i < k-1; i++) {
                currentColumn[i] = currentColumnAlmost[i];
            }
            ret.push_back(currentColumn);
        } while (it.nextCombo(currentColumnAlmost));
    }
    return ret;
}
