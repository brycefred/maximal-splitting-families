//
//  HyperCubeGraph.cpp
//  Maximal Splitting Families
//
//  Created by Bryce Frederickson on 2/8/19.
//  Copyright © 2019 Bryce Frederickson. All rights reserved.
//

#include "HyperCubeGraph.hpp"

int HyperCubeGraph::distance(int n, int u, int v) {
    int adj = u ^ v;
    int ret = 0;
    for (int i = 0; i < n; ++i) {
        if ((adj >> i) & 1) {
            ++ret;
        }
    }
    return ret;
}

int HyperCubeGraph::minimumDistanceLessThan(int family[], int n, int k, int min) {
    for (int vIndex = 1; vIndex < k; ++vIndex) {
        for (int uIndex = 0; uIndex < vIndex; ++uIndex) {
            if (distance(n,family[uIndex],family[vIndex]) < min) {
                return vIndex;
            }
        }
    }
    return -1;
}

bool HyperCubeGraph::isCopperSmith(int family[], int size) {
    bool ret = true;
    for (int i = 0; i < size; i++) {
        if (degree(family,size,i) != 2) {
            ret = false;
            break;
        }
    }
    return ret;
}

int HyperCubeGraph::degree(int graph[], int size, int vIndex) {
    int ret = 0;
    
    int vertex = graph[vIndex];
    for (int i = 0; i < size; i++) {
        int edge = vertex ^ graph[i];
        if (((edge & (edge - 1)) == 0) && edge != 0) {
            ret++;
        }
    }
    return ret;
}
