//
//  HyperCubeGraph.hpp
//  Maximal Splitting Families
//
//  Created by Bryce Frederickson on 2/8/19.
//  Copyright © 2019 Bryce Frederickson. All rights reserved.
//

#ifndef HyperCubeGraph_hpp
#define HyperCubeGraph_hpp

#include <stdio.h>

class HyperCubeGraph {
public:
    int distance(int n, int u, int v);
    int minimumDistanceLessThan(int family[], int n, int k, int min);
    bool isCopperSmith(int family[], int size);
    int degree(int graph[], int size, int vIndex);
};

#endif /* HyperCubeGraph_hpp */
