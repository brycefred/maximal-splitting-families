//
//  Iterator.hpp
//  Maximal Splitting Families
//
//  Created by Bryce Frederickson on 11/17/18.
//  Copyright © 2018 Bryce Frederickson. All rights reserved.
//

#ifndef Iterator_hpp
#define Iterator_hpp

#include <stdio.h>
#include <vector>

class Iterator {
public:
    bool nextCombo(int* combo, int n, int k);
    bool nextCombo(std::vector<bool> &combo);
};

#endif /* Iterator_hpp */
