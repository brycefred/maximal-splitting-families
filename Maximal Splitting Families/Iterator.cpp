//
//  Iterator.cpp
//  Maximal Splitting Families
//
//  Created by Bryce Frederickson on 11/17/18.
//  Copyright © 2018 Bryce Frederickson. All rights reserved.
//

#include "Iterator.hpp"

bool Iterator::nextCombo(int* combo, int n, int k) {
    bool ret = false;
    int startIndex = k + 1;
    int start = 0;
    for (int x = k - 1; x >=0; x--) {
        if (combo[x] < (n-1) - ((k-1)-x)) {
            startIndex = x;
            start = combo[x] + 1;
            ret = true;
            break;
        }
    }
    if (ret) {
        for (int x = startIndex; x < k; x++) {
            combo[x] = start + (x - startIndex);
        }
    }
    return ret;
}

bool Iterator::nextCombo(std::vector<bool> &combo) {
    bool ret = false;
    int n = static_cast<int>(combo.size());
    int k = 0;
    for (bool entry : combo) {
        if (entry) {
            k++;
        }
    }
    int howManyToMove1 = 0;
    for (int i = n - 1; i > 0; i--) {
        bool movable1 = (combo[i] == 0);
        bool somethingToMove1 = (combo[i-1] == 1);
        if (combo[i] == 1) {
            howManyToMove1++;
        }
        if ((i == 1) && !(somethingToMove1 || movable1)) {
            ret = false;
        }
        if (somethingToMove1 && movable1) {
            howManyToMove1++;
            combo[i-1] = 0;
            for (int i1 = i; i1 < n; i1++) {
                if (i1 < i + howManyToMove1) {
                    combo[i1] = 1;
                } else {
                    combo[i1] = 0;
                }
            }
            ret = true;
            break;
        }
    }
    return ret;
}
