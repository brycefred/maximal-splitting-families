//
//  Printer.hpp
//  Maximal Splitting Families
//
//  Created by Bryce Frederickson on 11/17/18.
//  Copyright © 2018 Bryce Frederickson. All rights reserved.
//

#ifndef Printer_hpp
#define Printer_hpp

#include <stdio.h>
#include <string>
#include "HyperCubeGraph.hpp"

class Printer {
private:
    HyperCubeGraph g;
public:
    void printFamily(int toPrint[], int n, int k);
    bool printFamilyToFileCoppersmith(int toPrint[], int n, int k, std::string fileName);
};

#endif /* Printer_hpp */
