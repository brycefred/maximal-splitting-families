//
//  Printer.cpp
//  Maximal Splitting Families
//
//  Created by Bryce Frederickson on 11/17/18.
//  Copyright © 2018 Bryce Frederickson. All rights reserved.
//

#include "Printer.hpp"
#include <iostream>
#include <fstream>

void Printer::printFamily(int toPrint[], int n, int k) {
    for (int j = 0; j < k; j++) {
        char set = 'a';
        bool empty = true;
        for (int i = 0; i < n; i++) {
            if ((toPrint[j] >> i) & 1) {
                std::cout << set;
                empty = false;
            }
            set++;
        }
        if (empty) {
            std::cout << '0';
        }
        std::cout << " ";
    }
    std::cout << "\n";
    std::cout << "Set sizes: ";
    for (int i = 0; i < n; i++) {
        int rowsum = 0;
        for (int j = 0; j < k; j++) {
            if ((toPrint[j] >> i) & 1) {
                rowsum++;
            }
        }
        std::cout << rowsum << " ";
    }
    std::cout << "\n";
}

bool Printer::printFamilyToFileCoppersmith(int toPrint[], int n, int k, std::string fileName) {
    std::ofstream fout;
    std::string address = fileName;
    fout.open(address, std::ios::app); //, std::ios::out | std::ios::app);
    if (fout.is_open()) {
        for (int j = 0; j < k; j++) {
            char set = 'a';
            bool empty = true;
            for (int i = 0; i < n; i++) {
                if ((toPrint[j] >> i) & 1) {
                    fout << set;
                    empty = false;
                }
                set++;
            }
            if (empty) {
                fout << '0';
            }
            fout << " ";
        }
        fout << "\n";
        fout << "Set sizes: ";
        for (int i = 0; i < n; i++) {
            int rowsum = 0;
            for (int j = 0; j < k; j++) {
                if ((toPrint[j] >> i) & 1) {
                    rowsum++;
                }
            }
            fout << rowsum << " ";
        }
        fout << "\n";
        fout << "Points: " << k << "\n";
        if (g.isCopperSmith(toPrint,k)) {
            fout << "Coppersmith\n";
        } else {
            fout << "Not Coppersmith!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
            << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
        }
        fout << "\n";
        fout.close();
        return true;
    } else {
        std::cout << "Error: File failed to open\n";
        return false;
    }
}
